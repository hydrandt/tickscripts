# tickscripts

Tickscripts - alerting on data in influxdb, collected by telegraf [TICK monitoring stack]

## alert-http-check.tick

Alerts on data collected by the http_response telegraf plugin. Allows to specify how many consecutive checks must fail ( -> result code of the plugin is higher than zero) to alert, and includes http response code in the alert message body.

# alert-service-running

Alerts on systemd service not running.
